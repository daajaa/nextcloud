% Nextcloud - Le cloud respectueux de nos données
% Genma
% Nextcloud

## A propos de moi - Genma

![Mon avatar](./images/avatar.png)

* **Un blog :** https://blog.genma.fr
* **Twitter :** @genma
* **Mastodon :** genma@framasphere.org
* **Membre de Framasoft** (*Degooglisons Internet*, Chatons...)
* **Utilisateur de logiciel libre depuis 2003**.
* **Yunohost && Nextcloud *Evangelist***

# Les problématiques du Cloud

## Le problème avec le cloud publique 1/2

### Vous n'avez pas le contrôle de vos données

* Pas de gestion de partage de fichiers.
* Monitoring et Administration incompatible avec les cloud publique.

### Vous ne savez pas où sont stockées les données

* Vous ne savez pas  où sont stockées les informations (GAFAM ? Chine ?)
* La juridiction des pays où se trouvent les serveurs peuvent avoir accès à vos données.
* RGPD compliant ?

### Un entonnoir pour vos données

* Vos données sont stockés au même endroit que d'autres entreprises.
* Ces serveurs sont plus susceptible d'être pirâtés pour leurs données.

## Le problème avec le cloud publique 2/2

### Aucun visibilité dans le cadre de piratage

* En cas d'attaque vous n'avez aucune visibilité.
* Vous ne pouvez effectuer aucune action immédiate pour limiter les dégats.

### Une solution pas totalement personnalisée

* Des solutions générique mais pas personnalisée et personnalisable.
* Pas de contrôle sur le système de *votre cloud*.
* Aucune contribution possible (Pas Open Source).

### L'anonymat de vos données (GAFAM)

* Vos données sont soumis aux juridictions du pays ou elles se trouvent.
* Espionnage industriel.

# NextCloud ?

## NextCloud

![](./images/NextCloud.png)

## Pourquoi NextCloud ?

### Les avantages

* Logiciel libre - open source
* Interface web moderne et application mobile simple d'utilisation.
* Contributions avec création ou améliorations des apps.
* Conforme aux normes (RGPD, Contrôle d'accès).

## NextCloud, un Groupware

* NextCloud est un groupware qui offre les outils nécessaire au bon développement de votre entreprise, en corrélation avec NextCloud Files il vous permets d'optimer vos processus internes.

## NextCloud Mail

* Client Mail avec support POP et IMAP serveurs ;
* Multiple comptes ;
* Gestionnaire de dossier ;
* Support du chiffrement de bout à bout.

![](./images/Nextcloud-Mail.png)

## NextCloud Calendar

* Création de multiple calendrier ;
* Prise en charge de calendriers externes ;
* Partager votre calendrier avec votre équipe ou rendez-le public.

![](./images/Nextcloud-Calendar.png)

## Nextcloud Contacts

* Partager vos contacts avec votre équipe ;
* Synchroniser vos contact avec votre téléphone et vos autres appareils.

![](./images/Nextcloud-Contact.png)

## NextCloud - Talk

* Appels audio & vidéo directement depuis le navigateur.

![](./images/Nextcloud-Talk.png)

## NextCloud - Deck

* Créer des kanban et partager-les avec votre équipe ;
* Déposer vos cartes, assignez-les aux membre de votre équipe.

![](./images/Nextcloud-Deck.png)

## NextCloud - Notes

* Application de prises de notes, compatible Markdown ;
* Synchronisation avec une applicaton sur téléphone, avec l'application *Fichiers*.

![](./images/Nextcloud-Notes.png)

## NextCloud Files

* Solution de synchronisation et de partage de fichiers d'entreprise avec une interface intuitive

![](./images/Nextcloud-Files.png)

# Nextcloud - Le store

## Nextcloud Store 1/2

Tout un tas d'autres applications pour venir compléter / enrichir Nextcloud :

![](./images/Nextcloud-Store01.png)

## Nextcloud Store 2/2

![](./images/Nextcloud-Store02.png)


# Nextcloud - Clients

## Nextcloud - Clients

**Android & IOS :**

![](./images/Nextcloud-Client_Android.png)

## Nextcloud - Clients

**Bureautique :**

![](./images/Nextcloud-Client_desktop.png)

# Nextcloud & la sécurité

## Nextcloud & la sécurité

### La sécurité au coeur de NextCloud 1/2

* Connexion annuaire LDAP.
* Chiffrement (SSL/TLS) pour le transfert de fichiers.
* Chiffrement AES-256 pour le stockage des fichiers.
* **Double authentification** (TOTP et U2F).
* Chiffrement de bout en bout.

### La sécurité au coeur de NextCloud 2/2

* Conformité au HIPAA et HITECH (Loi Santé) ;
* Conformité au RGPD ;
* Fonctionnalité d'Audit ;
* Contrôle d'accès aux fichiers.

# Nextcloud - Bureautique en ligne

## Collabora ?

* Une entreprise qui propose du support de niveau 3 pour LibreOffice ;
* Un des principals contributeurs à LibreOffice (1/3 du code).

### Collabora Online ?

* Collabora Online est une version de LOOL qui :
  * A un support sur le long terme LTS (Long Term Support),
  * Des mises à jours de sécurité validées et signées
  * Un accord de niveau de service SLA (Service Level Agreement)

### Collabora Online Developpment Edition

* Une version reposant sur la version en cours de développement de LibreOffice Online, mais est construite autour de Collabora Office core.

## Collabora - Fonctionnalités

### Formats supportés :

* Format OpenDocument (.odt, .odc...)

### Principales Fonctionnalités :

* Création de nouveaux documents ;
* Suivi des modifications et des versions ;
* **Edition simultanée et collaborative d'un même document** ;
* Partage du document.

## Collabora - quelques images

![](./images/Nextcloud-Collabora-2.png)

## Collabora - quelques images

![](./images/Nextcloud-Collabora-3.png)

## Collabora - quelques images

![](./images/Nextcloud-Collabora-4.png)

# Installer Nextcloud ?

## C'est assez simple

### Pour avoir son cloud personnel

Via Yunohost, c'est une des nombreuses applications disponibles.

### En *direct*

Un peu de *sysadmin* (LAMP : Linux, Apache, Mysql, PHP)

## Via une association

### Le collectifs CHATONS

*Les AMAP du manger bio de l'informatique.*

![](./images/logo-chatons-org.png)

# Nextcloud - Une offre professionnelle

## Professionnellement

### Actuellement

* Je travaille pour **Atos** comme Chef de projet - Architecte et Spécialiste Open source.

## Qu'est-ce que nous prévoyons de faire avec Nextcloud?

### Faire ce que nous savons déjà faire avec l'Open source

* Convaincre nos clients d’utiliser Nextcloud comme alternative à leurs *besoins de type cloud*.
* Installez Nextcloud sur leurs infrastructures ou héberger pour leurs comptes, les serveurs Nexctloud sur notre offre de cloud / datacenter.
* Leurs fournir du support.

**Créons une offre Nextcloud pour nos clients !**

## Avantages pour Nextcloud?

* Plus de clients et d'utilisateurs
* Rapports de bugs
* Contributions
   * Nouvelles applications
   * Amélioration des applications existantes

## La suite professionnellement ?

### Trouver des clients

* Terminer pour construire l'offre
* ... et la communication à ce sujet

### Partenariat

* Avec Nextcloud
* Avec Collabora
* Avec Onlyoffice

### Meetup

Nous prévoyons d'organiser un Nextcloud Meetup à Paris, en France.

Statut : Work In Progress

## Et Communautairement

### Nextcloudfr 

* Communauté NextcloudFR une initiative pour fédérer la communauté Nextcloud en France !
  * nextcloudfr@framalistes.org

# Merci de votre attention, place aux questions

# Annexes

# Applications

## Nextcloud - DashboardCharts

![](./images/Nextcloud-DashboardCharts.png)
