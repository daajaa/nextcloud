# Nextcloud-fr

* [La mailling liste nextcloud-fr](https://framalistes.org/sympa/info/nextcloudfr) et [les mails déjà publiés](https://framalistes.org/sympa/arc/nextcloudfr)
* [Pad de présentation des membres de la communauté](https://mensuel.framapad.org/p/nextcloudfr?lang=fr)

# Publication et blog

## Trouver des articles de blogs qui parlent de Nextcloud ?

* [Journal du Hacker - Filtre Nextcloud](https://www.journalduhacker.net/search?utf8=%E2%9C%93&q=nextcloud&what=all&order=newest)
* 
## Quelques sites et blogs en français

* [sebsauvage.net - Nextcloud](https://sebsauvage.net/wiki/doku.php?id=nextcloud)
* [lofurol.fr](https://lofurol.fr/joomla/component/search/?searchword=nextcloud)

# Technique - Quelles recettes pour déployer vos instances Nextcloud ?

## Installation

* [Nextcloud 17 installation guide (Debian 9/10) by Carsten Rieger](https://www.c-rieger.de/nextcloud-installation-guide-debian-9-10/)

## Ansible

* [ansible-roles-paquerette](https://gitlab.com/j.marchini/ansible-roles-paquerette) : Proposition of minimal ansible roles, to host and maintain services on premise or in the cloud. Supported systems : Ubuntu 16.04 LTS, Ubuntu 18.04 LTS
partially : debian 9
* [lab.enough.community - nextcloud.yml](https://lab.enough.community/main/infrastructure/blob/master/molecule/enough/roles/nextcloud/tasks/nextcloud.yml) Pour s'inspirer de ce que fait le lab.
* [zwindler/ansible-nextcloud](https://github.com/zwindler/ansible-nextcloud) Ansible playbooks for Ubuntu 18.04 (LEMP PHP7.2) 

## Puppet

* [Adullact - Vagrant Nextcloud](https://gitlab.adullact.net/adullact/vagrant-nextcloud)
* [Adullact - Puppet Nextcloud](https://gitlab.adullact.net/adullact/puppet-nextcloud)

# Wiki

## Framasoft - Degooglisons - Cultivons notre jardin

* [Framadrive / Nextcloud](https://docs.framasoft.org/fr/nextcloud/)
	* [Prise en main](https://https://docs.framasoft.org/fr/nextcloud/prise-en-main.html)
	* [Synchronisation des données](https://https://docs.framasoft.org/fr/nextcloud/synchro_clients.html)
	* [Gérer les appareils périphériques et les navigateurs connectés](https://https://docs.framasoft.org/fr/	nextcloud/session_management.html)
	* [Utiliser l'authentification en deux étapes](https://https://docs.framasoft.org/fr/nextcloud/User_2fa.html)           
	* [Régler les préférences utilisateur Définir vos préférences](https://https://docs.framasoft.org/fr/nextcloud/userpreferences.html)
	* [L'interface web de Nextcloud](https://https://docs.framasoft.org/fr/nextcloud/webinterface.html)

* [Framagenda](https://docs.framasoft.org/fr/agenda/framagenda)            
	* [Documentation pour Framagenda](https://docs.framasoft.org/fr/agenda/documentation-pour-framagenda)      
	* [Interface](https://docs.framasoft.org/fr/agenda/interface)       
	* [Inscription &amp; Connexion ](https://docs.framasoft.org/fr/agenda/inscription--connexion)
	* [Agenda](https://docs.framasoft.org/fr/agenda/agenda)
	* [Contacts](https://docs.framasoft.org/fr/agenda/contacts)
	* [Tâches](https://docs.framasoft.org/fr/agenda/tâches)
	* [Synchronisation avec un client ](https://docs.framasoft.org/fr/agenda/synchronisation-avec-un-client)
		* [Bureau](https://docs.framasoft.org/fr/agenda/bureau)
		* [Mobile](https://docs.framasoft.org/fr/agenda/mobile)
	* [Présentation vidéo](https://docs.framasoft.org/fr/agenda/présentation-vidéo)
	* [FAQ](https://docs.framasoft.org/fr/agenda/faq)

# Vidéos

## JRES - Journées Réseaux de l'Enseignement supérieur

Trois conférences de retour d'expérience de mise en place de Nextcloud et Onlyoffice sur des instances de dizaines de milliers d'utilisateurs.

Les liens vers les vidéos 2019 parlant de Nextcloud :
* [Le cloud collaboratif](https://replay.jres.org/videos/watch/0ff941d4-6219-487f-b3fe-3871acd510df) retour sur de l'Université de Lorraine
* [Drive RENATER](https://replay.jres.org/videos/watch/d40ec032-99d1-478b-b863-40c8e1c3ac82)
* [UNCloud, de 0 à 10 000 utilisateurs en 1 an](https://replay.jres.org/videos/watch/a13a023f-6243-48f5-9e35-dae6623c53a1) Retour sur la mise en place de Nextcloud par l'université de Nantes


## 36CCC Chaos Computer Club Congress

* [A dozen more things you didn't know Nextcloud could do](https://media.ccc.de/v/36c3-oio-160-a-dozen-more-things-you-didn-t-know-nextcloud-could-do#t=1067)