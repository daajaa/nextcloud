# Notes d'installation - Nextcloud avec Yunohost

Notes de configuration de Nextcloud hébergé par Yunohost.

## Upgrade de Nextcloud (v16 ou v17)

La version actuelle de Yunohost (v3.X) fonctionne avec la version de PHP 7.0 qui n'est plus supporté par les dernières versions de Nextcloud (versions 16 ou 17). Il faut donc installer PHP 7.1, 7.2 ou 7.3 en plus de l'actuelle 7.0.

L'astuce consiste donc à laisser Yunohost tourner sur sa version (PHP 7.0), mais de configurer Nextcloud pour qu'il travaille en PHP supérieur (ici PHP 7.3).

**Attention**, la méthode décrite ci-dessous vous coupe de l'installation automatisée de Nextcloud en passant par Yunohost.

<details>
    <summary>Installation de PHP 7.3</summary>

**Configuration de PHP 7.3**

```sh
sudo su -
wget -q -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.list
apt update
apt install php-curl php-imagick php7.3 php7.3-apcu php7.3-bcmath php7.3-curl php7.3-fpm php7.3-gd php7.3-intl php7.3-ldap php7.3-mbstring php7.3-mysql php7.3-xml php7.3-zip
update-alternatives --set php /usr/bin/php7.0
```

**Configuration de Nginx**

```sh
vim /etc/nginx/conf.d/ncloud.mon-site.org.d/nextcloud.conf
```

    #fastcgi_pass unix:/var/run/php/php7.0-fpm-nextcloud.sock;
    fastcgi_pass unix:/var/run/php/php7.3-fpm-nextcloud.sock;

**Configuration de php-fpm**

```sh
cp /etc/php/7.0/fpm/pool.d/nextcloud.conf /etc/php/7.3/fpm/pool.d/
rm -rf /etc/php/7.3/fpm/pool.d/www.conf
vim /etc/php/7.3/fpm/pool.d/nextcloud.conf
```

    listen = /var/run/php/php7.3-fpm-nextcloud.sock

Relancez les services

```sh
systemctl restart nginx
systemctl restart php7.3-fpm
```

</details>

<details>
    <summary>Mettre à jour Nextcloud</summary>

Configurez le canal de mise à jour.

```sh
vim /var/www/nextcloud/config/config.php
```

      'updatechecker' => true,
      'updater.release.channel' => 'stable',

Lancez la mise à jour

```sh
sudo -u nextcloud php7.3 updater/updater.phar
sudo -u nextcloud php7.3 occ upgrade
sudo -u nextcloud php7.3 occ maintenance:mode --off
```

Résultat :

![](media/capt-nextcloud_upgrade_v16.png)

</details>

## Avertissements de sécurité & configuration de Nextcloud

Dans la partie administration, la section "Avertissements de sécurité & configuration" vous permet d'optimiser Nextcloud pour obtenir une configuration optimale. Menu _Paramètres > Administration > Vue d'ensemble_

Voici quelques questions / réponses

<details>
    <summary>La base de données a quelques index manquant.</summary>

La base de données a quelques index manquant. L'ajout d'index dans de grandes tables peut prendre un certain temps. Elles ne sont donc pas ajoutées automatiquement. En exécutant "occ db:add-missing-indices", ces index manquants pourront être ajoutés manuellement pendant que l'instance continue de tourner. Une fois les index ajoutés, les requêtes sur ces tables sont généralement beaucoup plus rapides.

```
sudo -u nextcloud php7.3 occ db:add-missing-indices
```

</details>


<details>
    <summary>La limite de mémoire PHP est inférieure à la valeur recommandée de 512 Mo.</summary>

```
vim /etc/php/7.3/fpm/pool.d/nextcloud.conf
```

    php_value[upload_max_filesize] = 16G
    php_value[post_max_size] = 16G
    php_admin_value[memory_limit] = 512M

</details>


<details>
    <summary>Corriger les problèmes d'accès à .well-known/***dav</summary>

```
vim /etc/nginx/conf.d/ncloud.mon-site.org.d/nextcloud.conf
```
Ajoutez vers la fin du fichier :

      location = /.well-known/carddav {
        return 301 $scheme://$host:$server_port/remote.php/dav;
      }

      location = /.well-known/caldav {
        return 301 $scheme://$host:$server_port/remote.php/dav;
      }

</details>



--  
Janusz / 2019-11-10  
